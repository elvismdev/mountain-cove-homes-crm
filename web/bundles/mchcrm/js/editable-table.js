var EditableTable = function () {

    return {

        //main function to initiate the module
        init: function () {
            function restoreRow(oTable, nRow) {
                var aData = oTable.fnGetData(nRow);
                var jqTds = $('>td', nRow);

                for (var i = 0, iLen = jqTds.length; i < iLen; i++) {
                    oTable.fnUpdate(aData[i], nRow, i, false);
                }

                oTable.fnDraw();
            }

            function editRow(oTable, nRow, idedit) {
                var aData = oTable.fnGetData(nRow);
                var jqTds = $('>td', nRow);
                var id = oTable.fnSettings().fnRecordsTotal();

                jqTds[0].innerHTML = '<div class="input-group"><span class="input-group-addon"><input type="checkbox" class="checkbox" onclick="check(this, ' + id + ')"></span><input id="dateCollected_' + id + '" type="text" class="form-control default-date-picker small" data-mask="99/99/9999" value="' + aData[0] + '"></div><span class="help-inline" style="padding-left: 40px;">dd/mm/yyyy</span>';
                //jqTds[0].innerHTML = '<input type="text" class="form-control default-date-picker small" data-mask="99/99/9999" value="' + aData[0] + '">';
                jqTds[1].innerHTML = '<input type="text" class="form-control small" value="' + aData[1] + '">';
                jqTds[2].innerHTML = '<input type="text" class="form-control small" value="' + aData[2] + '">';
                jqTds[3].innerHTML = '<div class="input-group"><span class="input-group-addon">$</span><input type="text" class="form-control small" value="' + aData[3] + '"></div>';
                jqTds[4].innerHTML = '<input type="text" class="form-control small" value="' + aData[4] + '">';
                // jqTds[5].innerHTML = '<input type="text" class="form-control small" value="' + aData[5] + '">';
                jqTds[5].innerHTML = '<a id="'+idedit+'" class="edit" href="">Save</a>';
                jqTds[6].innerHTML = '<a class="cancel" href="">Cancel</a>';
            }

            function saveRow(oTable, nRow) {
                var count = document.getElementById('count').value;
                var jqInputs = $('input', nRow);
                oTable.fnUpdate(jqInputs[1].value, nRow, 0, false);
                oTable.fnUpdate(jqInputs[2].value, nRow, 1, false);
                oTable.fnUpdate(jqInputs[3].value, nRow, 2, false);
                oTable.fnUpdate(jqInputs[4].value, nRow, 3, false);
                oTable.fnUpdate(jqInputs[5].value, nRow, 4, false);
                // oTable.fnUpdate(jqInputs[6].value, nRow, 5, false);
                oTable.fnUpdate('<a class="edit" id="' + count + '" href="">Edit</a>', nRow, 5, false);
                oTable.fnUpdate('<a class="delete" id="' + count + '" href="">Delete</a>', nRow, 6, false);
                oTable.fnDraw();

                // Form reference:
                var theForm = document.forms['mch_contractsbundle_contract'];

                // Add data:
                addHidden(theForm, 'deposits[' + count + '][date_collected]', jqInputs[1].value, 'deposits' + count);
                addHidden(theForm, 'deposits[' + count + '][payment_type]', jqInputs[2].value, 'deposits' + count);
                addHidden(theForm, 'deposits[' + count + '][check_number]', jqInputs[3].value, 'deposits' + count);
                addHidden(theForm, 'deposits[' + count + '][amount]', jqInputs[4].value, 'deposits' + count);
                addHidden(theForm, 'deposits[' + count + '][for_]', jqInputs[5].value, 'deposits' + count);
                // addHidden(theForm, 'deposits[' + count + '][check_made_out_to]', jqInputs[6].value, 'deposits' + count);

                document.getElementById('count').setAttribute('value', parseInt(count) + 1);
            }

            function addHidden(theForm, key, value, inputClass) {
                // Create a hidden input element, and append it to the form:
                var input = document.createElement('input');
                input.type = 'hidden';
                input.name = key;
                input.value = value;
                input.className = inputClass;
                theForm.appendChild(input);
            }

            function cancelEditRow(oTable, nRow) {
                var jqInputs = $('input', nRow);
                oTable.fnUpdate(jqInputs[1].value, nRow, 0, false);
                oTable.fnUpdate(jqInputs[2].value, nRow, 1, false);
                oTable.fnUpdate(jqInputs[3].value, nRow, 2, false);
                oTable.fnUpdate(jqInputs[4].value, nRow, 3, false);
                oTable.fnUpdate(jqInputs[5].value, nRow, 4, false);
                // oTable.fnUpdate(jqInputs[6].value, nRow, 5, false);
                oTable.fnUpdate('<a class="edit" href="">Edit</a>', nRow, 6, false);
                oTable.fnDraw();
            }

            var oTable = $('#editable-sample').dataTable({
                "bPaginate": false,
                "bLengthChange": false,
                "bFilter": false,
                'bInfo': false,
                "bSort": false
                // "aLengthMenu": [
                //     [5, 15, 20, -1],
                //     [5, 15, 20, "All"] // change per page values here
                // ],
                // // set the initial value
                // "iDisplayLength": 5,
                // "sDom": "<'row'<'col-lg-6'l><'col-lg-6'f>r>t<'row'<'col-lg-6'i><'col-lg-6'p>>",
                // "sPaginationType": "bootstrap",
                // "oLanguage": {
                //     "sLengthMenu": "_MENU_ records per page",
                //     "oPaginate": {
                //         "sPrevious": "Prev",
                //         "sNext": "Next"
                //     }
                // },
                // "aoColumnDefs": [{
                //         'bSortable': false,
                //         'aTargets': [0]
                //     }
                // ]
            });

            jQuery('#editable-sample_wrapper .dataTables_filter input').addClass("form-control medium"); // modify table search input
            jQuery('#editable-sample_wrapper .dataTables_length select').addClass("form-control xsmall"); // modify table per page dropdown

            var nEditing = null;

            $('#editable-sample_new').click(function (e) {
                e.preventDefault();
                var aiNew = oTable.fnAddData(['', '', '', '', '', '',
                    '<a class="edit" href="">Edit</a>', '<a class="cancel" data-mode="new" href="">Cancel</a>'
                ]);
                var nRow = oTable.fnGetNodes(aiNew[0]);
                editRow(oTable, nRow);
                nEditing = nRow;
            });

            $('#editable-sample a.delete').live('click', function (e) {
                e.preventDefault();
                var count = document.getElementById('count').value;

                if (confirm("Are you sure to delete this row ?") == false) {
                    return;
                }

                var nRow = $(this).parents('tr')[0];
                oTable.fnDeleteRow(nRow);

                $('.deposits' + this.id).remove();
                document.getElementById('count').setAttribute('value', parseInt(count) - 1);
            });

            $('#editable-sample a.cancel').live('click', function (e) {
                e.preventDefault();
                if ($(this).attr("data-mode") == "new") {
                    var nRow = $(this).parents('tr')[0];
                    oTable.fnDeleteRow(nRow);
                } else {
                    restoreRow(oTable, nEditing);
                    nEditing = null;
                }
            });

            $('#editable-sample a.edit').live('click', function (e) {
                e.preventDefault();

                /* Get the row as a parent of the link that was clicked on */
                var nRow = $(this).parents('tr')[0];

                if (nEditing !== null && nEditing != nRow) {
                    /* Currently editing - but not this row - restore the old before continuing to edit mode */
                    restoreRow(oTable, nEditing);
                    editRow(oTable, nRow, this.id);
                    nEditing = nRow;
                } else if (nEditing == nRow && this.innerHTML == "Save") {
                    /* Editing this row and want to save it */
                    saveRow(oTable, nEditing);
                    nEditing = null;
                    $(".deposits"+this.id).remove();
                } else {
                    /* No edit in progress - let's start one */
                    editRow(oTable, nRow, this.id);
                    nEditing = nRow;
                }
            });
        }

    };

}();

function check(check, id) {
    if (check.checked) {
        $("#dateCollected_" + id).prop('disabled', true);
        $("#dateCollected_" + id).val('PENDING');
    } else {
        $("#dateCollected_" + id).prop('disabled', false);
        $("#dateCollected_" + id).val('');
    }
}