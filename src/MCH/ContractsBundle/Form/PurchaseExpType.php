<?php

namespace MCH\ContractsBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class PurchaseExpType extends AbstractType
{
    private $purchaseExp;
    private $properAccount;

    function __construct($purchaseExp=null, $properAccount=null)
    {
        $this->purchaseExp = $purchaseExp;
        $this->properAccount = $properAccount;
    }

    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('estClosingDate')
            ->add('sellerName')
            ->add('basePrice')
            ->add('lotPremium')
            ->add('options')
            ->add('basePriceIncentive')
            ->add('lotPremiumIncentive')
            ->add('optionIncentive')
            ->add('totalSalesPrice')
        ;
    }

    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'MCH\ContractsBundle\Entity\PurchaseExp'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'mch_contractsbundle_purchaseexp';
    }
}
