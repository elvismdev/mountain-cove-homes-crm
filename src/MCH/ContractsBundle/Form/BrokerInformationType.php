<?php

namespace MCH\ContractsBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class BrokerInformationType extends AbstractType
{
        /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
        public function buildForm(FormBuilderInterface $builder, array $options)
        {
            $builder
            ->add('realtorInSale', 'choice', array(
                'choices' => array(
                    'y' => 'Yes',
                    'n' => 'No'
                    ),
                'expanded' => true
                )
            )
            ->add('companyName')
            ->add('repName')
            ->add('address')
            ->add('telephone', 'text')
            ->add('commisionRate')
            ->add('bonusAmount')
            ->add('brokerRebate', 'choice', array(
                'choices' => array(
                    'y' => 'I WILL',
                    'n' => 'I WILL NOT'
                    ),
                'expanded' => true
                ))
            ;
        }

    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'MCH\ContractsBundle\Entity\BrokerInformation'
            ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'mch_contractsbundle_brokerinformation';
    }
}
