<?php

namespace MCH\ContractsBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class BuyerInformationType extends AbstractType
{
    private $checklist;

    function __construct($checklist=null)
    {
        $this->checklist = $checklist;
    }


    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
        ->add('buyers1')
        ->add('buyers2')
        ->add('buyersCurrentAddress')
        ->add('email')
        ->add('maritalStatus', 'choice', array(
            'empty_value' => 'Choose Status',
            'choices' => array(
                'married' => 'Married',
                'single' => 'Single',
                'divorced' => 'Divorced',
                'widowed' => 'Widowed',
                'separated' => 'Separated',
                'domestic_partner' => 'Domestic Partner'
                )
            )
        )
        ->add('workPhone')
        ->add('homePhone')
        ->add('mobilePhone')
        ;
    }

    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'MCH\ContractsBundle\Entity\BuyerInformation'
            ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'mch_contractsbundle_buyerinformation';
    }
}
