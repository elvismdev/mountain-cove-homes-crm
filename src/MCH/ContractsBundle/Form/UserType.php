<?php

namespace MCH\ContractsBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

use Doctrine\ORM\EntityRepository;

class UserType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('fullname', null, array('label' => 'fullname'))
            ->add('password', 'password', array('required' => false))
            ->add('email', 'email', array('label' => 'email'))
            ->add('position')
            ->add('twitter')
            ->add('linkedin')
            ->add('skype')
            ->add('address')
            ->add('phone')
            ->add('photo', 'file', array('required' => false, 'mapped' => false))
            ;
    }

    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'MCH\ContractsBundle\Entity\User'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'vess_contractsbundle_user';
    }
}
