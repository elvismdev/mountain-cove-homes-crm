<?php

namespace MCH\ContractsBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class ExplanationDepType extends AbstractType
{
        /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
        public function buildForm(FormBuilderInterface $builder, array $options)
        {
            $builder
            ->add('percentOfDeposit')
            ->add('mortagePrincipal')
            ->add('abaNumber')
            ->add('jobNumber')
            ->add('checksMadeOutTo', 'choice', array(
                'choices' => array(
                    'mch' => 'Mountain Cove Homes at South Dade, LLC',
                    'alexis' => 'Law Offices of Alexis Gonzalez, PA Trust Account',
                    'other' => 'Other'
                    ),
                'expanded' => true
                )
            )
            ;
        }

    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'MCH\ContractsBundle\Entity\ExplanationDep'
            ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'mch_contractsbundle_explanationdep';
    }
}
