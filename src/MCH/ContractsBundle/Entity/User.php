<?php

namespace MCH\ContractsBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Security\Core\Role\Role;
use Symfony\Component\Security\Core\User\AdvancedUserInterface;

/**
 * Usuario
 */
class User implements AdvancedUserInterface
{

    /**
     * @var integer
     */
    protected $id;

    protected $fullname;
    protected $password;
    protected $email;
    protected $salt;
    protected $rol;
    protected $status;

    public function getUsername()
    {
        return $this->email;
    }

    public function getNickname()
    {
        return $this->fullname;
    }

    /**
     * @inheritDoc
     */
    public function getSalt()
    {
        return $this->salt;
    }

    /**
     * @inheritDoc
     */
    public function getPassword()
    {
        return $this->password;
    }

    /**
     * @inheritDoc
     */
    public function eraseCredentials()
    {

    }

    public function getFullname()
    {
        return $this->fullname;
    }

    public function getEmail()
    {
        return $this->email;
    }

    public function getRoles()
    {
        return array($this->rol);
    }

    public function getRol()
    {
        return $this->rol;
    }

    /**
     * @inheritDoc
     */
    public function equals(AdvancedUserInterface $user)
    {
        return $this->username === $user->getUsername();
    }

    public function setPassword($pass)
    {
        $this->password = $pass;
    }

    public function setSalt($salt)
    {
        $this->salt = $salt;
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    public function setId(\MCH\ContractsBundle\Entity\User $id = null)
    {
        $this->id = $id;

        return $this;
    }

    public function setFullname($fullname)
    {
        $this->fullname = $fullname;

        return $this;
    }

    public function setEmail($email)
    {
        $this->email = $email;

        return $this;
    }

    public function setRoles($rol)
    {
        $this->rol = $rol;

        return $this;
    }

    public function setRol($rol)
    {
        $this->rol = $rol;

        return $this;
    }

    public function __toString()
    {
        return $this->username;
    }

    public function isPasswordLegal()
    {
        return strstr($this->password, $this->username);
    }

    public function setStatus($status)
    {
        $this->status = $status;

        return $this;
    }

    public function getStatus()
    {
        return $this->status;
    }

    public function isAccountNonExpired()
    {
        return $this->status;
    }

    public function isAccountNonLocked()
    {
        return $this->status;
    }

    public function isCredentialsNonExpired()
    {
        return $this->status;
    }

    public function isEnabled()
    {
        return $this->status;
    }
    /**
     * @var string
     */
    private $position;

    /**
     * @var string
     */
    private $twitter;

    /**
     * @var string
     */
    private $linkedin;

    /**
     * @var string
     */
    private $skype;

    /**
     * @var string
     */
    private $address;

    /**
     * @var integer
     */
    private $phone;


    /**
     * Set position
     *
     * @param string $position
     * @return User
     */
    public function setPosition($position)
    {
        $this->position = $position;

        return $this;
    }

    /**
     * Get position
     *
     * @return string
     */
    public function getPosition()
    {
        return $this->position;
    }

    /**
     * Set twitter
     *
     * @param string $twitter
     * @return User
     */
    public function setTwitter($twitter)
    {
        $this->twitter = $twitter;

        return $this;
    }

    /**
     * Get twitter
     *
     * @return string
     */
    public function getTwitter()
    {
        return $this->twitter;
    }

    /**
     * Set linkedin
     *
     * @param string $linkedin
     * @return User
     */
    public function setLinkedin($linkedin)
    {
        $this->linkedin = $linkedin;

        return $this;
    }

    /**
     * Get linkedin
     *
     * @return string
     */
    public function getLinkedin()
    {
        return $this->linkedin;
    }

    /**
     * Set skype
     *
     * @param string $skype
     * @return User
     */
    public function setSkype($skype)
    {
        $this->skype = $skype;

        return $this;
    }

    /**
     * Get skype
     *
     * @return string
     */
    public function getSkype()
    {
        return $this->skype;
    }

    /**
     * Set address
     *
     * @param string $address
     * @return User
     */
    public function setAddress($address)
    {
        $this->address = $address;

        return $this;
    }

    /**
     * Get address
     *
     * @return string
     */
    public function getAddress()
    {
        return $this->address;
    }

    /**
     * Set phone
     *
     * @param string $phone
     * @return User
     */
    public function setPhone($phone)
    {
        $this->phone = $phone;

        return $this;
    }

    /**
     * Get phone
     *
     * @return string
     */
    public function getPhone()
    {
        return $this->phone;
    }
    /**
     * @var string
     */
    private $photo;


    /**
     * Set photo
     *
     * @param string $photo
     * @return User
     */
    public function setPhoto($photo)
    {
        $this->photo = $photo;

        return $this;
    }

    /**
     * Get photo
     *
     * @return string
     */
    public function getPhoto()
    {
        return $this->photo;
    }
}
