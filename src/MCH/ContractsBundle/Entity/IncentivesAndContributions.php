<?php

namespace MCH\ContractsBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Class IncentivesAndContributions
 * @package MCH\ContractsBundle\Entity
 */
class IncentivesAndContributions {
    private $id;

    private $titleAndSettlementIncentiveAmount;

    private $titleAndSettlementIncentivePercentage;

    private $closingCostIncentiveAmount;

    private $closingCostIncentivePercentage;

    /**
     * @param mixed $closingCostIncentiveAmount
     */
    public function setClosingCostIncentiveAmount($closingCostIncentiveAmount)
    {
        $this->closingCostIncentiveAmount = $closingCostIncentiveAmount;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getClosingCostIncentiveAmount()
    {
        return $this->closingCostIncentiveAmount;
    }

    /**
     * @param mixed $closingCostIncentivePercentage
     */
    public function setClosingCostIncentivePercentage($closingCostIncentivePercentage)
    {
        $this->closingCostIncentivePercentage = $closingCostIncentivePercentage;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getClosingCostIncentivePercentage()
    {
        return $this->closingCostIncentivePercentage;
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $titleAndSettlementIncentiveAmount
     */
    public function setTitleAndSettlementIncentiveAmount($titleAndSettlementIncentiveAmount)
    {
        $this->titleAndSettlementIncentiveAmount = $titleAndSettlementIncentiveAmount;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getTitleAndSettlementIncentiveAmount()
    {
        return $this->titleAndSettlementIncentiveAmount;
    }

    /**
     * @param mixed $titleAndSettlementIncentivePercentage
     */
    public function setTitleAndSettlementIncentivePercentage($titleAndSettlementIncentivePercentage)
    {
        $this->titleAndSettlementIncentivePercentage = $titleAndSettlementIncentivePercentage;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getTitleAndSettlementIncentivePercentage()
    {
        return $this->titleAndSettlementIncentivePercentage;
    }
} 