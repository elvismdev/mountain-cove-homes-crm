<?php

namespace MCH\ContractsBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * BuyerInformation
 */
class BuyerInformation
{
    /**
     * @var integer
     */
    private $id;

    /**
     * @var string
     */
    private $buyers;

    /**
     * @var string
     */
    private $buyersCurrentAddress;

    /**
     * @var string
     */
    private $email;

    /**
     * @var string
     */
    private $maritalStatus;

    /**
     * @var string
     */
    private $workPhone;

    /**
     * @var string
     */
    private $homePhone;

    /**
     * @var string
     */
    private $mobilePhone;


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set buyers
     *
     * @param string $buyers
     * @return BuyerInformation
     */
    public function setBuyers($buyers)
    {
        $this->buyers = $buyers;

        return $this;
    }

    /**
     * Get buyers
     *
     * @return string
     */
    public function getBuyers()
    {
        return $this->buyers;
    }

    /**
     * Set buyersCurrentAddress
     *
     * @param string $buyersCurrentAddress
     * @return BuyerInformation
     */
    public function setBuyersCurrentAddress($buyersCurrentAddress)
    {
        $this->buyersCurrentAddress = $buyersCurrentAddress;

        return $this;
    }

    /**
     * Get buyersCurrentAddress
     *
     * @return string
     */
    public function getBuyersCurrentAddress()
    {
        return $this->buyersCurrentAddress;
    }

    /**
     * Set email
     *
     * @param string $email
     * @return BuyerInformation
     */
    public function setEmail($email)
    {
        $this->email = $email;

        return $this;
    }

    /**
     * Get email
     *
     * @return string
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * Set maritalStatus
     *
     * @param string $maritalStatus
     * @return BuyerInformation
     */
    public function setMaritalStatus($maritalStatus)
    {
        $this->maritalStatus = $maritalStatus;

        return $this;
    }

    /**
     * Get maritalStatus
     *
     * @return string
     */
    public function getMaritalStatus()
    {
        return $this->maritalStatus;
    }

    /**
     * Set workPhone
     *
     * @param string $workPhone
     * @return BuyerInformation
     */
    public function setWorkPhone($workPhone)
    {
        $this->workPhone = $workPhone;

        return $this;
    }

    /**
     * Get workPhone
     *
     * @return string
     */
    public function getWorkPhone()
    {
        return $this->workPhone;
    }

    /**
     * Set homePhone
     *
     * @param string $homePhone
     * @return BuyerInformation
     */
    public function setHomePhone($homePhone)
    {
        $this->homePhone = $homePhone;

        return $this;
    }

    /**
     * Get homePhone
     *
     * @return string
     */
    public function getHomePhone()
    {
        return $this->homePhone;
    }

    /**
     * Set mobilePhone
     *
     * @param string $mobilePhone
     * @return BuyerInformation
     */
    public function setMobilePhone($mobilePhone)
    {
        $this->mobilePhone = $mobilePhone;

        return $this;
    }

    /**
     * Get mobilePhone
     *
     * @return string
     */
    public function getMobilePhone()
    {
        return $this->mobilePhone;
    }

    /**
     * @var string
     */
    private $buyers1;

    /**
     * @var string
     */
    private $buyers2;


    /**
     * Set buyers1
     *
     * @param string $buyers1
     * @return BuyerInformation
     */
    public function setBuyers1($buyers1)
    {
        $this->buyers1 = $buyers1;

        return $this;
    }

    /**
     * Get buyers1
     *
     * @return string
     */
    public function getBuyers1()
    {
        return $this->buyers1;
    }

    /**
     * Set buyers2
     *
     * @param string $buyers2
     * @return BuyerInformation
     */
    public function setBuyers2($buyers2)
    {
        $this->buyers2 = $buyers2;

        return $this;
    }

    /**
     * Get buyers2
     *
     * @return string
     */
    public function getBuyers2()
    {
        return $this->buyers2;
    }
}
