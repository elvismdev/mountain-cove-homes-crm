<?php

namespace MCH\ContractsBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * BrokerInformation
 */
class BrokerInformation
{
    /**
     * @var integer
     */
    private $id;

    /**
     * @var string
     */
    private $realtorInSale;

    /**
     * @var string
     */
    private $companyName;

    /**
     * @var string
     */
    private $repName;

    /**
     * @var string
     */
    private $address;

    /**
     * @var integer
     */
    private $telephone;

    /**
     * @var integer
     */
    private $commisionRate;

    /**
     * @var integer
     */
    private $bonusAmount;

    /**
     * @var string
     */
    private $brokerRebate;


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set realtorInSale
     *
     * @param string $realtorInSale
     * @return BrokerInformation
     */
    public function setRealtorInSale($realtorInSale)
    {
        $this->realtorInSale = $realtorInSale;

        return $this;
    }

    /**
     * Get realtorInSale
     *
     * @return string
     */
    public function getRealtorInSale()
    {
        return $this->realtorInSale;
    }

    /**
     * Set companyName
     *
     * @param string $companyName
     * @return BrokerInformation
     */
    public function setCompanyName($companyName)
    {
        $this->companyName = $companyName;

        return $this;
    }

    /**
     * Get companyName
     *
     * @return string
     */
    public function getCompanyName()
    {
        return $this->companyName;
    }

    /**
     * Set repName
     *
     * @param string $repName
     * @return BrokerInformation
     */
    public function setRepName($repName)
    {
        $this->repName = $repName;

        return $this;
    }

    /**
     * Get repName
     *
     * @return string
     */
    public function getRepName()
    {
        return $this->repName;
    }

    /**
     * Set address
     *
     * @param string $address
     * @return BrokerInformation
     */
    public function setAddress($address)
    {
        $this->address = $address;

        return $this;
    }

    /**
     * Get address
     *
     * @return string
     */
    public function getAddress()
    {
        return $this->address;
    }

    /**
     * Set telephone
     *
     * @param integer $telephone
     * @return BrokerInformation
     */
    public function setTelephone($telephone)
    {
        $this->telephone = $telephone;

        return $this;
    }

    /**
     * Get telephone
     *
     * @return integer
     */
    public function getTelephone()
    {
        return $this->telephone;
    }

    /**
     * Set commisionRate
     *
     * @param integer $commisionRate
     * @return BrokerInformation
     */
    public function setCommisionRate($commisionRate)
    {
        $this->commisionRate = $commisionRate;

        return $this;
    }

    /**
     * Get commisionRate
     *
     * @return integer
     */
    public function getCommisionRate()
    {
        return $this->commisionRate;
    }

    /**
     * Set bonusAmount
     *
     * @param integer $bonusAmount
     * @return BrokerInformation
     */
    public function setBonusAmount($bonusAmount)
    {
        $this->bonusAmount = $bonusAmount;

        return $this;
    }

    /**
     * Get bonusAmount
     *
     * @return integer
     */
    public function getBonusAmount()
    {
        return $this->bonusAmount;
    }

    /**
     * Set brokerRebate
     *
     * @param string $brokerRebate
     * @return BrokerInformation
     */
    public function setBrokerRebate($brokerRebate)
    {
        $this->brokerRebate = $brokerRebate;

        return $this;
    }

    /**
     * Get brokerRebate
     *
     * @return string
     */
    public function getBrokerRebate()
    {
        return $this->brokerRebate;
    }
}
