<?php

namespace MCH\ContractsBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

class AddendumToModify {
    private $id;

    private $text;

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $text
     */
    public function setText($text)
    {
        $this->text = $text;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getText()
    {
        return $this->text;
    }
} 