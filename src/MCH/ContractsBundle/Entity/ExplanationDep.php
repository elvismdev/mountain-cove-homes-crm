<?php

namespace MCH\ContractsBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * ExplanationDep
 */
class ExplanationDep
{
    /**
     * @var integer
     */
    private $id;

    /**
     * @var integer
     */
    private $percentOfDeposit;

    /**
     * @var integer
     */
    private $mortagePrincipal;

    /**
     * @var integer
     */
    private $abaNumber;

    /**
     * @var string
     */
    private $jobNumber;

    /**
     * @var string
     */
    private $checksMadeOutTo;


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set percentOfDeposit
     *
     * @param integer $percentOfDeposit
     * @return ExplanationDep
     */
    public function setPercentOfDeposit($percentOfDeposit)
    {
        $this->percentOfDeposit = $percentOfDeposit;

        return $this;
    }

    /**
     * Get percentOfDeposit
     *
     * @return integer
     */
    public function getPercentOfDeposit()
    {
        return $this->percentOfDeposit;
    }

    /**
     * Set mortagePrincipal
     *
     * @param integer $mortagePrincipal
     * @return ExplanationDep
     */
    public function setMortagePrincipal($mortagePrincipal)
    {
        $this->mortagePrincipal = $mortagePrincipal;

        return $this;
    }

    /**
     * Get mortagePrincipal
     *
     * @return integer
     */
    public function getMortagePrincipal()
    {
        return $this->mortagePrincipal;
    }

    /**
     * Set abaNumber
     *
     * @param integer $abaNumber
     * @return ExplanationDep
     */
    public function setAbaNumber($abaNumber)
    {
        $this->abaNumber = $abaNumber;

        return $this;
    }

    /**
     * Get abaNumber
     *
     * @return integer
     */
    public function getAbaNumber()
    {
        return $this->abaNumber;
    }

    /**
     * Set jobNumber
     *
     * @param string $jobNumber
     * @return ExplanationDep
     */
    public function setJobNumber($jobNumber)
    {
        $this->jobNumber = $jobNumber;

        return $this;
    }

    /**
     * Get jobNumber
     *
     * @return string
     */
    public function getJobNumber()
    {
        return $this->jobNumber;
    }

    /**
     * Set checksMadeOutTo
     *
     * @param string $checksMadeOutTo
     * @return ExplanationDep
     */
    public function setChecksMadeOutTo($checksMadeOutTo)
    {
        $this->checksMadeOutTo = $checksMadeOutTo;

        return $this;
    }

    /**
     * Get checksMadeOutTo
     *
     * @return string
     */
    public function getChecksMadeOutTo()
    {
        return $this->checksMadeOutTo;
    }
}
