<?php
/**
 * Created by PhpStorm.
 * User: l3yr0y
 * Date: 12/08/14
 * Time: 12:11
 */

namespace MCH\ContractsBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

class TableOfDeposits {
    private $id;

    private $contractId;

    private $dateCollected;

    private $paymentType;

    private $checkNumber;

    private $amount;

    private $for;

    // private $checkMadeOutTo;

    /**
     * @param mixed $amount
     */
    public function setAmount($amount)
    {
        $this->amount = $amount;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getAmount()
    {
        return $this->amount;
    }

    // /**
    //  * @param mixed $checkMadeOutTo
    //  */
    // public function setCheckMadeOutTo($checkMadeOutTo)
    // {
    //     $this->checkMadeOutTo = $checkMadeOutTo;

    //     return $this;
    // }

    // /**
    //  * @return mixed
    //  */
    // public function getCheckMadeOutTo()
    // {
    //     return $this->checkMadeOutTo;
    // }

    /**
     * @param mixed $checkNumber
     */
    public function setCheckNumber($checkNumber)
    {
        $this->checkNumber = $checkNumber;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getCheckNumber()
    {
        return $this->checkNumber;
    }

    /**
     * @param mixed $contractId
     */
    public function setContractId(\MCH\ContractsBundle\Entity\Contract $contractId = null)
    {
        $this->contractId = $contractId;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getContractId()
    {
        return $this->contractId;
    }

    /**
     * @param mixed $dateCollected
     */
    public function setDateCollected($dateCollected)
    {
        $this->dateCollected = $dateCollected;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getDateCollected()
    {
        return $this->dateCollected;
    }

    /**
     * @param mixed $for
     */
    public function setFor($for)
    {
        $this->for = $for;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getFor()
    {
        return $this->for;
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $paymentType
     */
    public function setPaymentType($paymentType)
    {
        $this->paymentType = $paymentType;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getPaymentType()
    {
        return $this->paymentType;
    }


}