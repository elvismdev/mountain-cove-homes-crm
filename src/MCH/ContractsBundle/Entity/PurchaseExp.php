<?php

namespace MCH\ContractsBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * PurchaseExp
 */
class PurchaseExp
{
    /**
     * @var integer
     */
    private $id;

    /**
     * @var \DateTime
     */
    private $estClosingDate;

    /**
     * @var string
     */
    private $sellerName;

    /**
     * @var float
     */
    private $basePrice;

    /**
     * @var float
     */
    private $lotPremium;

    /**
     * @var float
     */
    private $options;

    /**
     * @var float
     */
    private $basePriceIncentive;

    /**
     * @var float
     */
    private $lotPremiumIncentive;

    /**
     * @var float
     */
    private $optionIncentive;

    /**
     * @var float
     */
    private $totalSalesPrice;


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set estClosingDate
     *
     * @param \DateTime $estClosingDate
     * @return PurchaseExp
     */
    public function setEstClosingDate($estClosingDate)
    {
        $this->estClosingDate = $estClosingDate;

        return $this;
    }

    /**
     * Get estClosingDate
     *
     * @return \DateTime
     */
    public function getEstClosingDate()
    {
        return $this->estClosingDate;
    }

    /**
     * Set sellerName
     *
     * @param string $sellerName
     * @return PurchaseExp
     */
    public function setSellerName($sellerName)
    {
        $this->sellerName = $sellerName;

        return $this;
    }

    /**
     * Get sellerName
     *
     * @return string
     */
    public function getSellerName()
    {
        return $this->sellerName;
    }

    /**
     * Set basePrice
     *
     * @param float $basePrice
     * @return PurchaseExp
     */
    public function setBasePrice($basePrice)
    {
        $this->basePrice = $basePrice;

        return $this;
    }

    /**
     * Get basePrice
     *
     * @return float
     */
    public function getBasePrice()
    {
        return $this->basePrice;
    }

    /**
     * Set lotPremium
     *
     * @param float $lotPremium
     * @return PurchaseExp
     */
    public function setLotPremium($lotPremium)
    {
        $this->lotPremium = $lotPremium;

        return $this;
    }

    /**
     * Get lotPremium
     *
     * @return float
     */
    public function getLotPremium()
    {
        return $this->lotPremium;
    }

    /**
     * Set options
     *
     * @param float $options
     * @return PurchaseExp
     */
    public function setOptions($options)
    {
        $this->options = $options;

        return $this;
    }

    /**
     * Get options
     *
     * @return float
     */
    public function getOptions()
    {
        return $this->options;
    }

    /**
     * Set basePriceIncentive
     *
     * @param float $basePriceIncentive
     * @return PurchaseExp
     */
    public function setBasePriceIncentive($basePriceIncentive)
    {
        $this->basePriceIncentive = $basePriceIncentive;

        return $this;
    }

    /**
     * Get basePriceIncentive
     *
     * @return float
     */
    public function getBasePriceIncentive()
    {
        return $this->basePriceIncentive;
    }

    /**
     * Set lotPremiumIncentive
     *
     * @param float $lotPremiumIncentive
     * @return PurchaseExp
     */
    public function setLotPremiumIncentive($lotPremiumIncentive)
    {
        $this->lotPremiumIncentive = $lotPremiumIncentive;

        return $this;
    }

    /**
     * Get lotPremiumIncentive
     *
     * @return float
     */
    public function getLotPremiumIncentive()
    {
        return $this->lotPremiumIncentive;
    }

    /**
     * Set optionIncentive
     *
     * @param float $optionIncentive
     * @return PurchaseExp
     */
    public function setOptionIncentive($optionIncentive)
    {
        $this->optionIncentive = $optionIncentive;

        return $this;
    }

    /**
     * Get optionIncentive
     *
     * @return float
     */
    public function getOptionIncentive()
    {
        return $this->optionIncentive;
    }

    /**
     * Set totalSalesPrice
     *
     * @param float $totalSalesPrice
     * @return PurchaseExp
     */
    public function setTotalSalesPrice($totalSalesPrice)
    {
        $this->totalSalesPrice = $totalSalesPrice;

        return $this;
    }

    /**
     * Get totalSalesPrice
     *
     * @return float
     */
    public function getTotalSalesPrice()
    {
        return $this->totalSalesPrice;
    }
}
