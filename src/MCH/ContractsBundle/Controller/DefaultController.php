<?php

namespace MCH\ContractsBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class DefaultController extends Controller
{
    public function indexAction()
    {
        return $this->render('MCHContractsBundle:Default:index.html.twig');
    }
}
