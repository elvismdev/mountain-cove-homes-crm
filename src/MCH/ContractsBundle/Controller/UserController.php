<?php

namespace MCH\ContractsBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

use MCH\ContractsBundle\Entity\User;
use MCH\ContractsBundle\Form\UserType;

/**
 * User controller.
 *
 */
class UserController extends Controller
{

    /**
     * Lists all User entities.
     *
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $entities = $em->getRepository('MCHContractsBundle:User')->findAll();

        return $this->render('MCHContractsBundle:User:index.html.twig', array(
            'entities' => $entities,
            ));
    }
    /**
     * Creates a new User entity.
     *
     */
    public function createAction(Request $request)
    {
        $entity = new User();
        $form = $this->createCreateForm($entity);
        $form->handleRequest($request);

        if ($form->isValid()) {
            // Default Settings
            $this->setSecurePassword($entity);
            $entity->setStatus(true); // Disabled default
            $entity->setRol('ROLE_USER');

            $em = $this->getDoctrine()->getManager();
            $em->persist($entity);
            $em->flush();

            // Photo
            $file = $request->files->get('vess_contractsbundle_user');
            if (isset($file['photo'])) {
                $file = $file['photo'];

                if ($file->getMimeType() != 'image/jpeg' && $file->getMimeType() != 'image/png') {
                    $this->get('session')->getFlashBag()->add(
                        'notice',
                        'Only png/jpg file allowed'
                        );

                    return $this->redirect($this->generateUrl('user_edit', array('id' => $entity->getId())));
                }

                // Save image file && remove old one
                $dir = $this->get('kernel')->getRootDir() . "/../web/bundles/mchcrm/users/";

                $ext = '.png';
                if ($file->guessExtension() == 'jpeg')
                    $ext = '.jpg';

                $file->move($dir, 'user-' . $entity->getId() . $ext);
                $entity->setPhoto('user-' . $entity->getId() . $ext);

                $em->persist($entity);
                $em->flush();
            }

            return $this->redirect($this->generateUrl('user'));
        }

        return $this->render('MCHContractsBundle:User:new.html.twig', array(
            'entity' => $entity,
            'form'   => $form->createView(),
            ));
    }

    /**
     * Creates a form to create a User entity.
     *
     * @param User $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createCreateForm(User $entity)
    {
        $form = $this->createForm(new UserType(), $entity, array(
            'action' => $this->generateUrl('user_create'),
            'method' => 'POST',
            ));

        $form->add('submit', 'submit', array('label' => 'Create'));

        return $form;
    }

    /**
     * Displays a form to create a new User entity.
     *
     */
    public function newAction()
    {
        $entity = new User();
        $form   = $this->createCreateForm($entity);

        return $this->render('MCHContractsBundle:User:new.html.twig', array(
            'entity' => $entity,
            'form'   => $form->createView(),
            ));
    }

    /**
     * Finds and displays a User entity.
     *
     */
    public function showAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('MCHContractsBundle:User')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find User entity.');
        }

        $deleteForm = $this->createDeleteForm($id);

        return $this->render('MCHContractsBundle:User:show.html.twig', array(
            'entity'      => $entity,
            'delete_form' => $deleteForm->createView(),
            ));
    }

    /**
     * Displays a form to edit an existing User entity.
     *
     */
    public function editAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('MCHContractsBundle:User')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find User entity.');
        }

        $editForm = $this->createEditForm($entity);
        $deleteForm = $this->createDeleteForm($id);

        return $this->render('MCHContractsBundle:User:edit.html.twig', array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
            ));
    }

    /**
    * Creates a form to edit a User entity.
    *
    * @param User $entity The entity
    *
    * @return \Symfony\Component\Form\Form The form
    */
    private function createEditForm(User $entity)
    {
        $form = $this->createForm(new UserType(), $entity, array(
            'action' => $this->generateUrl('user_update', array('id' => $entity->getId())),
            'method' => 'PUT',
            ));

        $form->add('submit', 'submit', array('label' => 'Update'));

        return $form;
    }
    /**
     * Edits an existing User entity.
     *
     */
    public function updateAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('MCHContractsBundle:User')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find User entity.');
        }

        $current_photo = $entity->getPhoto();

        $current_pass = $entity->getPassword();

        $deleteForm = $this->createDeleteForm($id);
        $editForm = $this->createEditForm($entity);
        $editForm->handleRequest($request);

        if ($editForm->isValid()) {
            // Changing password if needed
            if ($entity->getPassword() != "") {
                $this->setSecurePassword($entity);
            } else {
                $entity->setPassword($current_pass);
            }

            // Photo
            $file = $request->files->get('vess_contractsbundle_user');
            if (isset($file['photo'])) {
                $file = $file['photo'];

                if ($file->getMimeType() != 'image/jpeg' && $file->getMimeType() != 'image/png') {
                    $this->get('session')->getFlashBag()->add(
                        'notice',
                        'Only png/jpg file allowed'
                        );

                    return $this->redirect($this->generateUrl('user_edit', array('id' => $id)));
                }

                // Save image file && remove old one
                $dir = $this->get('kernel')->getRootDir() . "/../web/bundles/mchcrm/users/";

                if (file_exists($dir . $current_photo))
                    @unlink($dir . $current_photo);

                $ext = '.png';
                if ($file->guessExtension() == 'jpeg')
                    $ext = '.jpg';

                $file->move($dir, 'user-' . $id . $ext);
                $entity->setPhoto('user-' . $id . $ext);
            }

            $em->flush();

            return $this->redirect($this->generateUrl('user'));
        }

        return $this->render('MCHContractsBundle:User:edit.html.twig', array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
            ));
    }
    /**
     * Deletes a User entity.
     *
     */
    public function deleteAction(Request $request, $id)
    {
        $form = $this->createDeleteForm($id);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $entity = $em->getRepository('MCHContractsBundle:User')->find($id);

            if (!$entity) {
                throw $this->createNotFoundException('Unable to find User entity.');
            }

            $em->remove($entity);
            $em->flush();
        }

        return $this->redirect($this->generateUrl('user'));
    }

    /**
     * Creates a form to delete a User entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm($id)
    {
        return $this->createFormBuilder()
        ->setAction($this->generateUrl('user_delete', array('id' => $id)))
        ->setMethod('DELETE')
        ->add('submit', 'submit', array('label' => 'Delete'))
        ->getForm()
        ;
    }

    private function setSecurePassword(&$entity)
    {
        $entity->setSalt(md5(time()));
        $encoder = new \Symfony\Component\Security\Core\Encoder\MessageDigestPasswordEncoder('sha512', true, 10);
        $password = $encoder->encodePassword($entity->getPassword(), $entity->getSalt());
        $entity->setPassword($password);
    }
}
